<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\ProductRepository;
use Symfony\Component\HttpKernel\Exception\HttpException;
use MongoDB\Driver\Exception\BulkWriteException;

class ProductsController extends Controller
{
    protected $repository;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ProductRepository $repository)
    {
        $this->repository = $repository;
    }

    public function createHandler(Request $request)
    {
        try {
            $this->repository->createOne($request->input('name'), (int)$request->input('price'));
        } catch (BulkWriteException $e) {
            return response('Product already exists', 409);
        }
        return response('');
    }
}
